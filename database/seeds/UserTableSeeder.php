<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user')->insert([
            'name' => 'Administrator',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123456'),
        ]);
        DB::table('role')->insert([
            'name' => 'Administrator'
        ]);
        DB::table('role')->insert([
            'name' => 'Finance'
        ]);
        DB::table('role')->insert([
            'name' => 'Accounting'
        ]);
        DB::table('role')->insert([
            'name' => 'Sales'
        ]);
        DB::table('role')->insert([
            'name' => 'Management'
        ]);

        $role = DB::table('role')->where('name', 'Administrator')->first();
        $user = DB::table('user')->where('email', 'admin@gmail.com')->first();
        DB::table('role_user')->insert([
            'role_id' => $role->id,
            'user_id' => $user->id
        ]);
    }
}
