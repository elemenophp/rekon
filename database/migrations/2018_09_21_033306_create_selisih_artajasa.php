<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSelisihArtajasa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_selisih_artajasa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone', 50)->nullable();
            $table->date('tgl')->nullable()->index();
            $table->string('source', 50)->nullable();
            $table->integer('denom')->nullable();
            $table->string('tipe', 10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_selisih_artajasa');
    }
}
