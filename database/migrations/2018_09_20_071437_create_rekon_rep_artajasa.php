<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRekonRepArtajasa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_rekon_rep_artajasa', function (Blueprint $table) {
            $table->increments('id');
            $table->date('trx_date')->index();
            $table->time('trx_time');
            $table->string('phone_number', 20)->index();;
            $table->string('sn', 100)->index();;
            $table->string('kd_produk', 8)->index();;
            $table->string('amount', 10);
            $table->string('status_ca', 50);
            $table->string('status_biller', 50);
            $table->string('tindakan');
            $table->string('tindak_lanjut');
            $table->string('upload');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_rekon_rep_artajasa');
    }
}
