<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksiHis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi_his', function (Blueprint $table) {
            $table->increments('IDTRANSAKSI');
            $table->string('IDRESELLER', 16)->nullable()->index();
            $table->string('NAMARESELLER', 50)->nullable()->index();
            $table->integer('IDPRODUK')->nullable()->index();
            $table->string('KODEPRODUK', 20)->nullable()->index();
            $table->integer('NOMINAL')->nullable();
            $table->string('KODEINBOXCENTER', 15)->nullable()->index();
            $table->date('TANGGAL')->nullable()->index();
            $table->time('JAM')->nullable();
            $table->string('TUJUAN', 100)->nullable()->index();
            $table->double('HARGABELI')->nullable();
            $table->double('HARGAJUAL')->nullable();
            $table->char('JENISTRANSAKSI', 1)->nullable();
            $table->integer('STATUSTRANSAKSI')->nullable()->index();
            $table->char('FLAGPESANSENDER', 1)->nullable();
            $table->char('SUDAHKIRIMSMSPESANMENUNGGUJAWABANOTOMATIS', 1)->nullable();
            $table->integer('COUNTER')->nullable();
            $table->integer('IDTERMINAL')->nullable()->index();
            $table->string('NAMATERMINAL', 150)->nullable();
            $table->double('HARGAJUALRESELLER')->nullable();
            $table->text('KETERANGAN')->nullable();
            $table->string('SN')->nullable();
            $table->char('JENISKOMISI', 1)->nullable();
            $table->string('NOPENGIRIM')->nullable();
            $table->integer('JUMKIRIMSMSBERSABAR')->nullable();
            $table->integer('JUMULANG')->nullable();
            $table->char('PATOKANHARGAJUAL', 1)->nullable();
            $table->date('TANGGALTERIMA')->nullable()->index();
            $table->time('JAMTERIMA')->nullable();
            $table->string('IDTRANSAKSICLIENT', 20)->nullable()->index();
            $table->integer('LEBOHDARI1X')->nullable();
            $table->string('PINOPERATOR', 10)->nullable()->index();
            $table->integer('JUMLAHUNIT')->nullable();
            $table->double('SALDOAWAL')->nullable();
            $table->double('SALDOAKHIR')->nullable();
            $table->string('NAMAAREA', 50)->nullable();
            $table->double('STOK')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_his');
    }
}
