<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRekonSwiArtajasa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_rekon_swi_artajasa', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('trx_time')->nullable();
            $table->string('kd_produk', 8)->index()->nullable();
            $table->string('kd_jenis', 10)->index()->nullable();
            $table->text('produk')->nullable();
            $table->string('mitra_bill', 8)->nullable();
            $table->string('id_pelanggan', 20)->nullable();
            $table->integer('bill')->nullable();
            $table->string('reff_pay', 30)->nullable();
            $table->integer('tagihan')->nullable();
            $table->integer('fee')->nullable();
            $table->integer('debet_beli')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_rekon_swi_artajasa');
    }
}
