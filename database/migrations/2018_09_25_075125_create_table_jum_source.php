<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableJumSource extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_jum_source', function (Blueprint $table) {
            $table->increments('id');
            $table->date('tgl')->nullable();
            $table->string('source', 50)->nullable();
            $table->integer('hrg_satuan')->nullable();
            $table->integer('jum_sukses')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_jum_source');
    }
}
