<div class="sidebar">
    <div class="sidebar-inner">
        <div class="sidebar-logo">
            <div class="peers ai-c fxw-nw">
                <div class="peer peer-greed"><a class="sidebar-link td-n" href="/"
                        class="td-n">
                        <div class="peers ai-c fxw-nw">
                            <div class="peer">
                                <div class="logo"><img src="/images/logo.png" alt=""></div>
                            </div>
                            <div class="peer peer-greed">
                                <h5 class="lh-1 mB-0 logo-text">Rekonsiliasi</h5>
                            </div>
                        </div>
                    </a></div>
                <div class="peer">
                    <div class="mobile-toggle sidebar-toggle"><a href="" class="td-n"><i class="ti-arrow-circle-left"></i></a></div>
                </div>
            </div>
        </div>
        <ul class="sidebar-menu scrollable pos-r">
            <li class="nav-item mT-30 active"><a class="sidebar-link" href="#"
                    default><span class="icon-holder"><i class="c-blue-500 ti-home"></i> </span><span class="title">Dashboard</span></a></li>
            <li class="nav-item dropdown"><a class="dropdown-toggle" href="javascript:void(0);"><span class="icon-holder"><i
                            class="c-teal-500 ti-layout-list-thumb"></i> </span><span class="title">Management User</span>
                    <span class="arrow"><i class="ti-angle-right"></i></span></a>
                <ul class="dropdown-menu">
                    <li class="nav-item dropdown"><a href="{{url('user')}}"><span>User Aplikasi</span></a></li>
                    <!-- <li class="nav-item dropdown"><a href="{{url('role')}}"><span>User Role</span></a></li> -->
                    <!-- <li class="nav-item dropdown"><a href="javascript:void(0);"><span>Privilage Menu</span></a></li> -->
                </ul>
            </li>

            <li class="nav-item dropdown"><a class="dropdown-toggle" href="javascript:void(0);"><span class="icon-holder"><i
                            class="c-teal-500 ti-layout-list-thumb"></i> </span><span class="title">Rekon Artajasa</span>
                    <span class="arrow"><i class="ti-angle-right"></i></span></a>
                <ul class="dropdown-menu">
                    <li class="nav-item dropdown"><a href="{{url('artajasa/report')}}"><span>Report Selisih</span></a></li>
                    <li class="nav-item dropdown"><a href="{{url('artajasa/upload')}}"><span>Upload File</span></a></li>
                    <li class="nav-item dropdown"><a href="{{url('artajasa/file')}}"><span>File</span></a></li>
                </ul>
            </li>

            <li class="nav-item dropdown"><a class="dropdown-toggle" href="javascript:void(0);"><span class="icon-holder"><i
                            class="c-teal-500 ti-layout-list-thumb"></i> </span><span class="title">Laporan Penjualan</span>
                    <span class="arrow"><i class="ti-angle-right"></i></span></a>
                <ul class="dropdown-menu">
                    <li class="nav-item dropdown"><a href="{{url('laporan/penjualan')}}"><span>Penjualan Bulanan</span></a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
