@extends('layouts.app')
@section('title', 'Laporan Bulanan')
@section('content')

<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20">
                        <h6 class="c-grey-900">Report Rekon</h6>
                        <p>Report data rekonsiliasi.</p>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <div id="message-error"></div>
                                <div class="form-group row" style="padding: 0 15px">
                                    <label class="col-form-label">Tanggal Awal</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control datepicker" id="start-date">
                                    </div>
                                    <div class="col-sm-4">
                                        <button id="btn-search-data" type="button" class="btn btn-primary">Cari Data</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h6 class="c-grey-900">Report Rekon</h6>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <div id="message-error"></div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <table class="table table-hover table-bordered" style="font-size: 10px">
                                            <thead>
                                                <tr>
                                                    <th scope="col" style="text-align: center" colspan="12">LAPORAN PENJUALAN BULANAN</th>
                                                </tr>
                                                <tr>
                                                    <th scope="col" width="40">No.</th>
                                                    <th scope="col">Supplier</th>
                                                    <th scope="col">Nama Terminal</th>
                                                    <th scope="col">Nama Operator</th>
                                                    <th scope="col">Nama Produk</th>
                                                    <th scope="col">Kode Produk</th>
                                                    <th scope="col">Profile</th>
                                                    <th scope="col">Sukses</th>
                                                    <th scope="col">Harga Beli Satuan</th>
                                                    <th scope="col">Harga Jual Satuan</th>
                                                    <th scope="col">Harga Beli</th>
                                                    <th scope="col">Harga Jual</th>
                                                </tr>
                                            </thead>
                                            <tbody id="retport">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>

    </div>
</main>
@push('scripts')
<script>
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy'
    });
    $(function () {
        $("#btn-search-data").on("click", function () {

            $("#message-error").empty();
            var startDate = $('#start-date').datepicker('getDate');
            if (startDate == null) {
                $("#message-error").append('<div class="alert alert-danger" role="alert">tanggal tidak valid</div>');
                return;
            }
            var formatStartDate = moment(startDate).format('YYYY-MM-DD');
            axios.post('/api/laporan/penjualan', {
                date: formatStartDate
            }).then((response) => {
                if (response.data.code == 400) {
                    var html =`<div class="alert alert-danger" role="alert">${response.data.message}</div>`;
                    $("#message-error").append(html);
                } else {
                    $("#retport").empty();
                    $.map(response.data.message, (row, index) => {
                        console.log(row);
                        
                        $("#retport").append(
                            `<tr>
                                <td scope="col" widtd="40">${index + 1}</td>
                                <td scope="col" style="text-align: center">${index}</td>
                                <td scope="col" style="text-align: right">${row.namaterminal}</td>
                                <td scope="col" style="text-align: right">${row.namaoperator}</td>
                                <td scope="col" style="text-align: right">${row.namaproduk}</td>
                                <td scope="col" style="text-align: right">${row.kodeproduk}</td>
                                <td scope="col" style="text-align: right">${index}</td>
                                <td scope="col" style="text-align: right">${row.jumlah}</td>
                                <td scope="col" style="text-align: right">${accounting.formatNumber(row.hargabeli)}</td>
                                <td scope="col" style="text-align: right">${accounting.formatNumber(row.hargajual)}</td>
                                <td scope="col" style="text-align: right">${accounting.formatNumber(row.hargabeli * row.jumlah)}</td>
                                <td scope="col" style="text-align: right">${accounting.formatNumber(row.hargajual * row.jumlah)}</td>
                            </tr>`
                        );
                    });

                    // footer report 1
                    $("#retport").append(
                        `<tr>
                            <td scope="col" colspan="3" style="text-align: right"><b>Total</b></td>
                            <td scope="col" style="text-align: right"><b>TOTAL</b></td>
                        </tr>`
                    );

                }
            });
        });
    });

</script>
@endpush
@endsection
