@extends('layouts.app')
@section('title', 'User Aplikasi')
@section('content')

<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20">
                        <h6 class="c-grey-900">User Aplikasi</h6>
                        <p>Membuat user aplikas baru untuk memberi hak akses ke aplikasi rekonsiliasi</p>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <a href="{{ url('user/new') }}" class="btn btn-primary">Tambah User Aplikasi</a>
                            </div>
                        </div>
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col" width="40">No.</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col" style="text-align: center">#</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $key => $row)
                                    <tr>
                                        <th style="text-align: center">{{ $key  + $users->firstItem() }}</th>
                                        <td>{{ $row->name }}</td>
                                        <td>{{ $row->email }}</td>
                                        <th scope="row" style="text-align: center">
                                        @if($row->name != 'Administrator')
                                            <a href="{{ url('user/update') }}?id={{$row->id}}" class="btn cur-p btn-outline-success btn-small">Update</a>
                                            <a href="{{ url('user/delete') }}?id={{$row->id}}" class="btn cur-p btn-outline-danger btn-small">Delete</a>
                                        @endif
                                        </th>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $users->links() }}
                    </div>
                </div>
            </div>
        </div>

    </div>
</main>

@endsection
