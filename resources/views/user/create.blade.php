@extends('layouts.app')
@section('title', 'Tambah User Aplikasi')
@section('content')

<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="row gap-20 masonry pos-r" style="position: relative; height: 1107px;">
            <div class="masonry-sizer col-md-6"></div>
            <div class="masonry-item col-md-12">
                <div class="bgc-white p-20 bd">
                    <h6 class="c-grey-900">Tambah User Aplikasi</h6>
                    <div class="mT-30">
                        <form action="{{ url('user/new') }}" method="POST">
                            {{ csrf_field() }}

                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label for="exampleInputEmail1">Name</label>
                                    <input type="text" name="name" class="form-control" value="{{old('name')}}">
                                    @if($errors->user->first('name'))
                                    <small class="form-text text-muted">{{ $errors->user->first('name') }}.</small>
                                    @endif
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="exampleInputEmail1">Email</label>
                                    <input type="text" name="email" class="form-control" value="{{old('email')}}">
                                    @if($errors->user->first('email'))
                                    <small class="form-text text-muted">{{ $errors->user->first('email') }}.</small>
                                    @endif
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="exampleInputEmail1">Password</label>
                                    <input type="text" name="password" class="form-control">
                                    @if($errors->user->first('password'))
                                    <small class="form-text text-muted">{{ $errors->user->first('password') }}.</small>
                                    @endif
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="exampleInputEmail1">Role User</label>
                                    <select id="inputState" class="form-control" name="role">
                                        <option selected="selected" value="">Pilih Role</option>
                                        @foreach($roles as $key => $row)
                                            <option value="{{$row->id}}">{{$row->name}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->user->first('name'))
                                        <small class="form-text text-muted">{{ $errors->user->first('name') }}.</small>
                                    @endif
                                </div>

                            </div>
                            
                            <button type="submit" class="btn btn-primary">Simpan Data</button>
                        </form>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</main>

@endsection
