@extends('layouts.app')
@section('title', 'Role User')
@section('content')

<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20">
                        <h6 class="c-grey-900">File</h6>
                        <p>Data file transaksi artajasa yang di upload manual ke server.</p>
                        <div class="form-group row">
                            <div class="col-sm-10">
                            </div>
                        </div>
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col" width="40">No.</th>
                                    <th scope="col">Tanggal</th>
                                    <th scope="col">File Name</th>
                                    <th scope="col" style="text-align: center">#</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($rekons as $key => $row)
                                    <tr>
                                        <th style="text-align: center">{{ $key  + $rekons->firstItem() }}</th>
                                        <td>{{ $row->tgl }}</td>
                                        <td>{{ $row->path }}</td>
                                        <th scope="row" style="text-align: center">
                                        @if($row->name != 'Administrator')
                                            <a href="{{ url('role/view') }}?id={{$row->id}}" class="btn cur-p btn-outline-success btn-small">Detail</a>
                                        @endif
                                        </th>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $rekons->links() }}
                    </div>
                </div>
            </div>
        </div>

    </div>
</main>

@endsection
