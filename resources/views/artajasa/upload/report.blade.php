@extends('layouts.app')
@section('title', 'Role User')
@section('content')

<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20">
                        <h6 class="c-grey-900">Report Rekon</h6>
                        <p>Report data rekonsiliasi.</p>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <div id="message-error"></div>
                                <div class="form-group row" style="padding: 0 15px">
                                    <label class="col-form-label">Tanggal Awal</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control datepicker" id="start-date">
                                    </div>
                                    <div class="col-sm-4">
                                        <button id="btn-search-data" type="button" class="btn btn-primary">Cari Data</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h6 class="c-grey-900">Report Rekon</h6>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <div id="message-error"></div>
                                <div class="form-group row">
                                    <div class="col-sm-6" style="padding-right: 0">
                                        <table class="table table-hover table-bordered" style="font-size: 12px">
                                            <thead>
                                                <tr>
                                                    <th scope="col" style="text-align: center" colspan="4">ARTAJASA</th>
                                                </tr>
                                                <tr>
                                                    <th scope="col" width="40">No.</th>
                                                    <th scope="col">Sukses</th>
                                                    <th scope="col">Harga</th>
                                                    <th scope="col">Jumlah</th>
                                                </tr>
                                            </thead>
                                            <tbody id="retport1">
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-sm-6" style="padding-left: 0">
                                        <table class="table table-hover table-bordered" style="font-size: 12px">
                                            <thead>
                                                <tr>
                                                    <th scope="col" style="text-align: center" colspan="4">SWITCHING</th>
                                                </tr>
                                                <tr>
                                                    <th scope="col" width="40">No.</th>
                                                    <th scope="col">Sukses</th>
                                                    <th scope="col">Harga</th>
                                                    <th scope="col">Jumlah</th>
                                                </tr>
                                            </thead>
                                            <tbody id="retport2">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <h6 class="c-grey-900">Selisih Report Rekon</h6>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <div id="message-error"></div>
                                <div class="form-group row">
                                    <div class="col-sm-6" style="padding-right: 0">
                                        <table class="table table-hover table-bordered" style="font-size: 12px">
                                            <thead>
                                                <tr>
                                                    <th scope="col" style="text-align: center" colspan="4">ARTAJASA</th>
                                                </tr>
                                                <tr>
                                                    <th scope="col" width="40">No.</th>
                                                    <th scope="col">No Telepon</th>
                                                    <th scope="col">Denom</th>
                                                </tr>
                                            </thead>
                                            <tbody id="selisih1">
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-sm-6" style="padding-left: 0">
                                        <table class="table table-hover table-bordered" style="font-size: 12px">
                                            <thead>
                                                <tr>
                                                    <th scope="col" style="text-align: center" colspan="4">SWITCHING</th>
                                                </tr>
                                                <tr>
                                                    <th scope="col" width="40">No.</th>
                                                    <th scope="col">No Telepon</th>
                                                    <th scope="col">Denom</th>
                                                </tr>
                                            </thead>
                                            <tbody id="selisih2">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</main>
@push('scripts')
<script>
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy'
    });
    $(function () {
        $("#btn-search-data").on("click", function () {

            $("#message-error").empty();
            var startDate = $('#start-date').datepicker('getDate');
            var formatStartDate = moment(startDate).format('YYYY-MM-DD');
            
            axios.post('/artajasa/get-report', {
                startdate: formatStartDate
            }).then((response) => {
                if (response.data.code == 400) {
                    var html =`<div class="alert alert-danger" role="alert">${response.data.message}</div>`;
                    $("#message-error").append(html);
                } else {
                    var total1 = 0;
                    
                    $("#retport1").empty();
                    $.map(response.data.message.report1, (row, index) => {
                        var jumlahTotal1 = row.jum_sukses * row.hrg_satuan;
                        $("#retport1").append(
                            `<tr>
                                <td scope="col" widtd="40">${index + 1}</td>
                                <td scope="col" style="text-align: center">${row.jum_sukses}</td>
                                <td scope="col" style="text-align: right">${accounting.formatNumber(row.hrg_satuan)}</td>
                                <td scope="col" style="text-align: right">${accounting.formatNumber(jumlahTotal1)}</td>
                            </tr>`
                        );
                        total1 = total1 + jumlahTotal1;
                    });

                    $("#retport2").empty();
                    var total2 = 0;
                    $.map(response.data.message.report2, (row, index) => {
                        var jumlahTotal2 = row.jum_sukses * row.hrg_satuan;
                        $("#retport2").append(
                            `<tr>
                                <td scope="col" widtd="40">${index + 1}</td>
                                <td scope="col" style="text-align: center">${row.jum_sukses}</td>
                                <td scope="col" style="text-align: right">${accounting.formatNumber(row.hrg_satuan)}</td>
                                <td scope="col" style="text-align: right">${accounting.formatNumber(jumlahTotal2)}</td>
                            </tr>`
                        );
                        total2 = total2 + jumlahTotal2;
                    });

                    // footer report 1
                    $("#retport1").append(
                        `<tr>
                            <td scope="col" colspan="3" style="text-align: right"><b>Total</b></td>
                            <td scope="col" style="text-align: right"><b>${accounting.formatNumber(total1)}</b></td>
                        </tr>`
                    );

                    // footer report 2
                    $("#retport2").append(
                        `<tr>
                            <td scope="col" colspan="3" style="text-align: right"><b>Total</b></td>
                            <td scope="col" style="text-align: right"><b>${accounting.formatNumber(total2)}</b></td>
                        </tr>`
                    );

                    // REPORT SELISIH
                    $("#selisih1").empty();
                    $.map(response.data.message.selisih1, (row, index) => {
                        $("#selisih1").append(
                            `<tr>
                                <td scope="col" widtd="40">${index + 1}</td>
                                <td scope="col" style="text-align: center">${row.phone}</td>
                                <td scope="col" style="text-align: right">${accounting.formatNumber(row.denom)}</td>
                            </tr>`
                        );
                    });

                    $("#selisih2").empty();
                    $.map(response.data.message.selisih2, (row, index) => {
                        $("#selisih2").append(
                            `<tr>
                                <td scope="col" widtd="40">${index + 1}</td>
                                <td scope="col" style="text-align: center">${row.phone}</td>
                                <td scope="col" style="text-align: right">${accounting.formatNumber(row.denom)}</td>
                            </tr>`
                        );
                    });

                }
            });
        });
    });

</script>
@endpush
@endsection
