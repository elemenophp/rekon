@extends('layouts.app')
@section('title', 'Role User')
@section('content')

<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20">
                        <h6 class="c-grey-900">Upload File Rekon</h6>
                        <p>Fitur untuk upload file rekonsiliasi artajasa</p>
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="form-group row">
                            <form class="form" method="POST" action="{{ url('artajasa/upload') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input id="file" type="file" name="file" name="myfile" accept=".txt">
                                        @if($errors->upload->first('file'))
                                            <small class="form-text text-muted">{{ $errors->upload->first('file') }}.</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <button class="btn btn-primary" type="submit" id="btn_upload_file">Upload File</button>
                                    </div>
                                </div>
                                
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</main>
@endsection
