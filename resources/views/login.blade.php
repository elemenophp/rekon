<!DOCTYPE html>
<html>

<head>
    <title>Login Rekonsiliasi App</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/reset.css') }}" rel="stylesheet">
</head>

<body class="app">
    <div id="loader">
        <div class="spinner"></div>
    </div>
    <script type="text/javascript">
        window.addEventListener('load', () => {
            const loader = document.getElementById('loader');
            setTimeout(() => {
                loader.classList.add('fadeOut');
            }, 100);
        });
    </script>
    <div class="peers ai-s fxw-nw h-100vh">
        <div class="d-n@sm- peer peer-greed h-100 pos-r bgr-n bgpX-c bgpY-c bgsz-cv" style="background-image:url(images/bg.jpg)">
            <div class="pos-a centerXY">
            </div>
        </div>
        <div class="col-12 col-md-4 peer pX-40 pY-80 h-100 bgc-white scrollable pos-r" style="min-width:320px">
            <h4 class="fw-300 c-grey-900 mB-40">Login Rekonsiliasi App</h4>
            <form action="{{route('doLogin')}}" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="text-normal text-dark">Email</label>
                    <input type="text" name="email" class="form-control" placeholder="Email" autocomplete="off" value="{{old('email')}}">
                    @if($errors->login->first('email'))
                        <div class="invalid-feedback">{{ $errors->login->first('email') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    <label class="text-normal text-dark">Password</label>
                    <input type="password" name="password" class="form-control" placeholder="Password" autocomplete="off">
                    @if($errors->login->first('password'))
                        <div class="invalid-feedback">{{ $errors->login->first('password') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    <div class="peers ai-c jc-sb fxw-nw">
                        <div class="peer">
                            <div class="checkbox checkbox-circle checkbox-info peers ai-c">
                                <input type="checkbox" name="remember" value="remember">
                                <label class="peers peer-greed js-sb ai-c">
                                <span class="peer peer-greed">Remember Me</span></label>
                            </div>
                        </div>
                        <div class="peer"><button type="submit" class="btn btn-primary">Login</button></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script type="text/javascript" src="{{asset('js/app.js') }}"></script>
</body>

</html>
