@extends('layouts.app')
@section('title', 'Role User')
@section('content')

<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20">
                        <h6 class="c-grey-900">User Role</h6>
                        <p>Fitur untuk memberikan level user atau memberikan hak akses ke user</p>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <a href="{{ url('role/new') }}" class="btn btn-primary">Tambah Role User</a>
                            </div>
                        </div>
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col" width="40">No.</th>
                                    <th scope="col">Role Name</th>
                                    <th scope="col" style="text-align: center">#</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($roles as $key => $row)
                                    <tr>
                                        <th style="text-align: center">{{ $key  + $roles->firstItem() }}</th>
                                        <td>{{ $row->name }}</td>
                                        <th scope="row" style="text-align: center">
                                        @if($row->name != 'Administrator')
                                            <a href="{{ url('role/update') }}?id={{$row->id}}" class="btn cur-p btn-outline-success btn-small">Update</a>
                                            <a href="{{ url('role/delete') }}?id={{$row->id}}" class="btn cur-p btn-outline-danger btn-small">Delete</a>
                                        @endif
                                        </th>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $roles->links() }}
                    </div>
                </div>
            </div>
        </div>

    </div>
</main>

@endsection
