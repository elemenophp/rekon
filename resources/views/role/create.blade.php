@extends('layouts.app')
@section('title', 'Role User')
@section('content')

<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="row gap-20 masonry pos-r" style="position: relative; height: 1107px;">
            <div class="masonry-sizer col-md-6"></div>
            <div class="masonry-item col-md-6" style="position: absolute; left: 0%; top: 0px;">
                <div class="bgc-white p-20 bd">
                    <h6 class="c-grey-900">Tambah Role User</h6>
                    <div class="mT-30">
                        <form action="{{ url('role/new') }}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="exampleInputEmail1">Role Name</label>
                                <input type="text" name="name" class="form-control" placeholder="contoh : Accounting">
                                @if($errors->role->first('name'))
                                <small id="emailHelp" class="form-text text-muted">{{ $errors->role->first('name') }}.</small>
                                @endif
                            </div>
                            
                            <button type="submit" class="btn btn-primary">Simpan Data</button>
                        </form>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</main>

@endsection
