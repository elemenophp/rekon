<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('isAccounting', function($user, $post){
            return $user->user_type == 'isAccounting';
        });

        Gate::define('isSales', function($user, $post){
            return $user->user_type == 'isSales';
        });

        Gate::define('isFinance', function($user, $post){
            return $user->user_type == 'isFinance';
        });

        Gate::define('isManagement', function($user, $post){
            return $user->user_type == 'isManagement';
        });
    }
}
