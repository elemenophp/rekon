<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    
    public $table = "menu";

    public static function getParentMenu()
    {
        return self::where('parent_id', 0)->get();
    }

}
