<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UploadArtajasa extends Model
{

    public $table = "tb_upload_artajasa";
    public $fillable = [
        'id', 'tgl', 'path', 'created_at', 'updated_at'
    ];

}
