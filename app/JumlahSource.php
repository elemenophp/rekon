<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JumlahSource extends Model
{
    
    public $table = "tb_jum_source";
    public $timestamps = false;

}
