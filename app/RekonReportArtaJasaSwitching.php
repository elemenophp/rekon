<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RekonReportArtaJasaSwitching extends Model
{
    
    public $table = "tb_rekon_swi_artajasa";
    public $fillable = [
        'id', 'trx_time', 'kd_produk', 'kd_jenis', 'produk', 'mitra_bill',
        'id_pelanggan', 'bill', 'reff_pay', 'tagihan', 'fee', 'debet_beli',
        'status', 'created_at', 'updated_at'
    ];

}
