<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RekonReportArtaJasa extends Model
{

    public $table = "tb_rekon_rep_artajasa";
    public $fillable = [
        'id', 'trx_date', 'trx_time', 'phone_number', 'sn', 'kd_produk',
        'amount', 'status_ca', 'status_biller', 'tindakan', 'tindak_lanjut', 'created_at', 'updated_at'
    ];

}
