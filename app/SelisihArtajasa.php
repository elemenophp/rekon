<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SelisihArtajasa extends Model
{
    
    public $table = "tb_selisih_artajasa";
    public $timestamps = false;

}
