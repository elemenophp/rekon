<?php

namespace App\Http\Controllers\Laporan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;
use DB;

class PenjualanBulananController extends Controller
{
    
    public function index()
    {
        return view('laporan.bulanan');
    }

    public function getSalesRepotMonthly(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date'    => 'required|date',
        ]);

        if ($validator->fails()) {
            foreach ($validator->getMessageBag()->toArray() as $key => $value) {
                return $this->jsonApi(400, $value[0]);
            }
        }

        $date = $request->get('date');

        $transaksi = DB::connection('slave_irs')
        ->table('transaksi_his AS his')
        ->selectRaw('produk.namaproduk, ter.namaterminal, operator.namaoperator, produk.idproduk, his.kodeproduk, his.nominal, his.hargabeli, his.hargajual, his.idterminal, COUNT(his.idtransaksi) as jumlah, (his.hargabeli * COUNT(his.idtransaksi)) as total')
        ->join('terminal AS ter', function($sql){
            $sql->on('his.idterminal', '=', 'ter.idterminal');
        })
        ->join('produk', function($sql){
            $sql->on('his.idproduk', '=', 'produk.idproduk');
        })
        ->join('operator', function($sql){
            $sql->on('produk.idoperator', '=', 'operator.idoperator');
        })
        ->where('TANGGAL', $date)
        ->where('STATUSTRANSAKSI', 1)
        ->whereIn('his.idterminal', [797, 529])
        ->groupBy('operator.namaoperator', 'produk.namaproduk', 'ter.namaterminal', 'idproduk', 'kodeproduk', 'nominal', 'hargabeli', 'hargajual', 'idterminal')
        ->orderBy('ter.namaterminal', 'his.nominal')
        ->get();

        return $this->jsonApi(200, $transaksi);
    }

}
