<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function jsonApi($code = 200, $message = null, $data = null){
        if ($data) {
          return response()->json([
            'code'      => $code,
            'message'   => $message,
            'data'      => $data
          ]);
        } else {
          return response()->json([
            'code'      => $code,
            'message'   => $message,
          ]);
        }
      }
      
}
