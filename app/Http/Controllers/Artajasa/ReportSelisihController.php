<?php

namespace App\Http\Controllers\Artajasa;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Validator;

use App\RekonReportArtaJasa;
use App\RekonReportArtaJasaSwitching;
use App\SelisihArtajasa;
use App\JumlahSource;

class ReportSelisihController extends Controller
{
    
    public function index()
    {
        return view('artajasa.upload.report');
    }

    public function getDataRekonsiliasi(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'startdate'    => 'required|date',
        ]);

        if ($validator->fails()) {
            foreach ($validator->getMessageBag()->toArray() as $key => $value) {
                return $this->jsonApi(400, $value[0]);
            }
        }

        $startdate = $request->get('startdate');
        $enddate = $request->get('enddate');

        $report1 = JumlahSource::whereDate('tgl', $startdate)
        ->where('source', 'artajasa')
        ->orderBy('hrg_satuan', 'asc')
        ->get();

        $report2 = JumlahSource::whereDate('tgl', $startdate)
        ->where('source', 'switching')
        ->orderBy('hrg_satuan', 'asc')
        ->get();

        $selisih1 = SelisihArtajasa::whereDate('tgl', $startdate)
        ->where('tipe', 'sa')
        ->orderBy('denom', 'asc')
        ->get();

        $selisih2 = SelisihArtajasa::whereDate('tgl', $startdate)
        ->where('tipe', 'as')
        ->orderBy('denom', 'asc')
        ->get();

        $query = [
            'report1'   => $report1,
            'report2'   => $report2,
            'selisih1'  => $selisih1,
            'selisih2'  => $selisih2,
        ];
        
        return $this->jsonApi(200, $query);
    }

    public function check(Request $request)
    {
        $rekons = DB::table('tb_rekon_swi_artajasa')
        ->selectRaw('DATE_FORMAT(trx_time, "%Y-%m-%d") AS tgl, debet_beli, SUM(status) AS jum_status')
        ->whereDate('trx_time', '2018-09-17')
        ->where('status', 1)
        ->groupBy('debet_beli', DB::raw('DATE_FORMAT(trx_time, "%Y-%m-%d")'))
        ->get();

        return $rekons;

        $listPelanggan = DB::table('tb_rekon_swi_artajasa')
        ->where('status', 1)
        ->whereDate('trx_time', '2018-09-03')
        ->pluck('id_pelanggan');

        $listReport = RekonReportArtaJasa::selectRaw('phone_number, trx_date, amount')
        ->where('trx_date', '2018-09-16')
        ->whereNotIn('phone_number', $listPelanggan)
        ->get();

        SelisihArtajasa::where('tgl', '2018-09-16')->delete();
        foreach ($listReport as $key => $value) {
            $selisih = new SelisihArtajasa;
            $selisih->phone     = $value->phone_number;
            $selisih->tgl       = $value->trx_date;
            $selisih->source    = 'report_artajasa vs switching';
            $selisih->denom     = $value->amount;
            $selisih->tipe      = 'sa';
            $selisih->save();
        }

        return $listReport;
    }
}
