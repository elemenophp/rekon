<?php

namespace App\Http\Controllers\Artajasa;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use DB;
use App\RekonReportArtaJasa;

use App\UploadArtajasa;
use App\SelisihArtajasa;
use App\RekonReportArtaJasaSwitching;
use App\JumlahSource;

class UploadFileController extends Controller
{
    
    public function index()
    {
        return view('artajasa.upload.index');
    }

    public function file(Request $request)
    {
        $rekons = UploadArtajasa::orderBy('tgl', 'desc')->paginate();
        return view('artajasa.upload.file')
        ->with('rekons', $rekons);
    }

    public function doUploadFile(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'file'    => 'required|file:txt',
            ]);
            if ($validator->fails()) {
                return redirect()
                ->bacK()
                ->withErrors($validator, 'upload')
                ->withInput();
            }
    
            if( $request->has('file') ) {
                $file = $request->file('file');
                $filename = 'RPT_HASIL_REKON_' . time() . '.' . $file->getClientOriginalExtension();
                $path = $file->storeAs('rekon/artajasa', $filename);
                $storage = storage_path('app/'.$path);
    
                $fileResource  = fopen($storage, "r");
                $collection = collect();
                while ( ($line = fgets($fileResource) ) !== false) {
                    if( !empty(trim($line)) ) {
                        $collection->push($line);
                    }
                }
                fclose($fileResource);

                DB::beginTransaction();
                $isDeletedReport = false;
                $index = 0;
                $isCurrentDateUpload = date('Y-m-d');
                foreach ($collection as $key => $value) {

                    if (strpos($value, "KODE PRODUK")) {
                        $index = $key + 2;
                    }
                    
                    if( $key > $index && $index != 0 && $isDeletedReport == false){
                        $obj = $this->convertToObject($value, $filename);
                        $isCurrentDateUpload = $obj['trx_date'];
                        $isDeletedReport = true;
                    }
                }

                $upload = new UploadArtajasa;
                $upload->tgl  = $isCurrentDateUpload;
                $upload->path = $path;
                $upload->save();

                RekonReportArtaJasa::where('trx_date', $isCurrentDateUpload)->delete();
                foreach ($collection as $key => $value) {
                    if( $key > $index && $index != 0 ){
                        $obj = $this->convertToObject($value, $filename);
                        if ( $obj ){
                            RekonReportArtaJasa::create($obj);
                        }
                    }
                }

                // start insert data ke table jum source
                $this->insertIntoTableSource($isCurrentDateUpload);
                // end insert data ke table jum source

                $listPelanggan = DB::table('tb_rekon_rep_artajasa')
                ->where('status_biller', 'Berhasil')
                ->whereDate('trx_date', $isCurrentDateUpload)
                ->pluck('phone_number');

                // start insert into selisih artajasa vs switching
                $this->insertIntoTableSelisihArtajasa($listPelanggan, $isCurrentDateUpload);
                // end insert into selisih artajasa vs switching

                $listPelangganSwi = DB::table('tb_rekon_swi_artajasa')
                ->where('status', 1)
                ->whereDate('trx_time', $isCurrentDateUpload)
                ->pluck('id_pelanggan');
                // start insert into selisih switching vs artajasa
                $this->insertIntoTableSelisihSwitching($listPelangganSwi, $isCurrentDateUpload);
                // end insert into selisih switching vs artajasa

                DB::commit();
            }
            
            return redirect('artajasa/upload')->with('status', 'Upload File Berhasil');
        } catch (Exception $e) {
            DB::rollback();
            return redirect('artajasa/upload')->with('status', 'Upload File Gagal');
        }
    }

    public function convertToObject($arrays, $path)
    {
        $objects = explode('|', $arrays);
        if( isset($objects[1]) ) {
            $date = date_create($objects[1]);
            $formatDate = date_format($date,"Y-m-d");
            return [
                'trx_date'      => $formatDate,
                'trx_time'      => trim($objects[2]),
                'phone_number'  => trim($objects[3]),
                'sn'            => trim($objects[4]),
                'kd_produk'     => trim($objects[5]),
                'amount'        => trim($objects[6]),
                'status_ca'     => trim($objects[7]),
                'status_biller' => trim($objects[8]),
                'tindakan'      => trim($objects[9]),
                'tindak_lanjut' => trim($objects[10]),
                'upload'        => $path,
            ];
        }
        return null;
    }

    public function insertIntoTableSource($date)
    {
        $rekons = DB::table('tb_rekon_swi_artajasa')
        ->selectRaw('DATE_FORMAT(trx_time, "%Y-%m-%d") AS tgl, debet_beli, SUM(status) AS jum_status')
        ->whereDate('trx_time', $date)
        ->where('status', 1)
        ->groupBy('debet_beli', DB::raw('DATE_FORMAT(trx_time, "%Y-%m-%d")'))
        ->get();

        JumlahSource::where('tgl', $date)->where('source', 'artajasa')->delete();
        foreach ($rekons as $key => $value) {
            $source = new JumlahSource;    
            $source->tgl        = $value->tgl;
            $source->source     = "artajasa";
            $source->hrg_satuan = $value->debet_beli;
            $source->jum_sukses = $value->jum_status;
            $source->save();
        }
    }

    public function insertIntoTableSelisihArtajasa($listPelanggan, $isCurrentDateUpload)
    {
        $listReport = RekonReportArtaJasa::selectRaw('phone_number, trx_date, amount')
                ->whereDate('trx_date', $isCurrentDateUpload)
                ->whereNotIn('phone_number', $listPelanggan)
                ->get();
                
        SelisihArtajasa::where('tgl', $isCurrentDateUpload)->where('tipe', 'sa')->delete();
        foreach ($listReport as $key => $value) {
            $selisih = new SelisihArtajasa;
            $selisih->phone     = $value->phone_number;
            $selisih->tgl       = $value->trx_date;
            $selisih->source    = 'switching vs report artajasa';
            $selisih->denom     = $value->amount;
            $selisih->tipe      = 'sa';
            $selisih->save();
        }
    }

    public function insertIntoTableSelisihSwitching($listPelanggan, $isCurrentDateUpload)
    {
        $listReport = RekonReportArtaJasaSwitching::selectRaw('id_pelanggan, trx_time, debet_beli')
                ->whereDate('trx_time', $isCurrentDateUpload)
                ->whereNotIn('id_pelanggan', $listPelanggan)
                ->where('status', 1)
                ->get();
                
        SelisihArtajasa::where('tgl', $isCurrentDateUpload)->where('tipe', 'as')->delete();
        foreach ($listReport as $key => $value) {
            $selisih = new SelisihArtajasa;
            $selisih->phone     = $value->id_pelanggan;
            $selisih->tgl       = $value->trx_time;
            $selisih->source    = 'report artajasa vs switching';
            $selisih->denom     = $value->debet_beli;
            $selisih->tipe      = 'as';
            $selisih->save();
        }   
    }

}
