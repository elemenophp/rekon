<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

class UserController extends Controller
{
    
    public function login()
    {
        return view('login');
    }

    public function doLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'     => 'required|email|max:100',
            'password'  => 'required|max:100',
        ]);

        if ($validator->fails()) {
            return redirect('login')
                        ->withErrors($validator, 'login')
                        ->withInput();
        }

        $credentials    = $request->only('email', 'password');
        $remember       = $request->get('remember');
        if($remember) {
            $remember = 1;
        } else {
            $remember = 0;
        }
        if( auth()->attempt($credentials, $remember) ) {
            return redirect('/');
        }

        if( auth()->viaRemember() ) {
            return redirect('/');
        }

        return redirect('/login')
        ->withErrors($validator, 'login')
        ->withInput();
    }

}
