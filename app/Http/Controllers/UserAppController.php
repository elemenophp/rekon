<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\User;
use App\Role;
use App\RoleUser;

class UserAppController extends Controller
{
    

    public function index()
    {
        $users = User::paginate(10);
        return view('user.index')->with('users', $users);
    }

    public function create()
    {
        $roles = Role::where('name', '!=', 'Administrator')->get();
        return view('user.create')->with('roles', $roles);
    }

    public function doCreate(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name'      => 'required|max:100',
                'email'     => 'required|unique:user,email|max:100',
                'password'  => 'required|max:100',
                'role'      => 'required',
            ]);
    
            if ($validator->fails()) {
                return redirect()
                ->back()
                ->withErrors($validator, 'user')
                ->withInput();
            }

            DB::beginTransaction();
    
            $user = new User;
            $user->name     = $request->get('name');
            $user->email    = $request->get('email');
            $user->password = $request->get('password');
            $user->save();
    
            $roleUser = new RoleUser;
            $roleUser->user_id = $user->id;
            $roleUser->role_id = $request->get('role');
            $roleUser->save();
            
            DB::commit();
            return redirect('user');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('status', $e->getMessage());
        }
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'    => 'required|exists:user,id',
        ]);

        if ($validator->fails()) {
            return redirect()
            ->back()
            ->withErrors($validator, 'user')
            ->withInput();
        }
        $roles = Role::where('name', '!=', 'Administrator')->get();
        $user = User::find($request->get('id'));
        return view('user.update')->with('user', $user)->with('roles', $roles);
    }

    public function doUpdate(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'id'        => 'required|exists:user,id',
                'name'      => 'required|max:100',
                'email'     => 'unique:user,email,'.$request->get('id'),
                'password'  => 'required|max:100',
                'role'      => 'required|exists:role,id',
            ]);
    
            if ($validator->fails()) {
                return redirect()
                ->back()
                ->withErrors($validator, 'user')
                ->withInput();
            }

            DB::beginTransaction();
    
            $user = User::find($request->get('id'));
            $user->name     = $request->get('name');
            $user->email    = $request->get('email');
            $user->password = $request->get('password');
            $user->save();
    
            RoleUser::where('user_id', $user->id)->delete();
            $roleUser = new RoleUser;
            $roleUser->user_id = $user->id;
            $roleUser->role_id = $request->get('role');
            $roleUser->save();
            
            DB::commit();
            return redirect('user');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('status', $e->getMessage());
        }
    }

    public function doDelete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'    => 'required|exists:user,id',
        ]);
        if ($validator->fails()) {
            return redirect()
            ->back()
            ->withErrors($validator, 'user')
            ->withInput();
        }
        Role::destroy($request->get('id'));
        return redirect('user');   
    }

}
