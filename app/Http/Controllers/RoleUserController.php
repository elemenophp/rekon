<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Role;

class RoleUserController extends Controller
{
    
    public function index()
    {
        $roles = Role::paginate(10);
        return view('role.index', ['roles' => $roles]);
    }

    public function create()
    {
        return view('role.create');
    }

    public function doCreate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required|unique:role,name|max:100',
        ]);

        if ($validator->fails()) {
            return redirect('role/new')
                        ->withErrors($validator, 'role')
                        ->withInput();
        }

        $role = new Role;
        $role->name = $request->get('name');
        $role->save();
        
        return redirect('role');
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'    => 'required|exists:role,id',
        ]);

        if ($validator->fails()) {
            return redirect('role/update')
                        ->withErrors($validator, 'role')
                        ->withInput();
        }
        $role = Role::find($request->get('id'));
        return view('role.update', ['role' => $role]);   
    }

    public function doUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'    => 'required|exists:role,id',
            'name'  => 'required|unique:role,name|max:100',
        ]);

        if ($validator->fails()) {
            return redirect()
            ->back()
            ->withErrors($validator, 'role')
            ->withInput();
        }

        $role = Role::find($request->get('id'));
        $role->name = $request->get('name');
        $role->save();
        
        return redirect('role');   
    }

    public function doDelete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'    => 'required|exists:role,id',
        ]);
        if ($validator->fails()) {
            return redirect('role')
                        ->withErrors($validator, 'role')
                        ->withInput();
        }
        Role::destroy($request->get('id'));
        return redirect('role');   
    }
    
}
