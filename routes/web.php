<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->middleware('auth');
Route::get('/logout', 'HomeController@logout')->middleware('auth');
Route::get('/login', 'UserController@login')->name('login');
Route::post('/login', 'UserController@doLogin')->name('doLogin');

// ROLE USER
Route::get('/role', 'RoleUserController@index')->middleware('auth');
Route::get('/role/new', 'RoleUserController@create')->middleware('auth');
Route::post('/role/new', 'RoleUserController@doCreate')->middleware('auth');
Route::get('/role/update', 'RoleUserController@update')->middleware('auth');
Route::post('/role/update', 'RoleUserController@doUpdate')->middleware('auth');
Route::get('/role/delete', 'RoleUserController@doDelete')->middleware('auth');

// USER APLIKASI
Route::get('/user', 'UserAppController@index')->middleware('auth');
Route::get('/user/new', 'UserAppController@create')->middleware('auth');
Route::post('/user/new', 'UserAppController@doCreate')->middleware('auth');
Route::get('/user/update', 'UserAppController@update')->middleware('auth');
Route::post('/user/update', 'UserAppController@doUpdate')->middleware('auth');
Route::get('/user/delete', 'UserAppController@doDelete')->middleware('auth');

// ARTAJASA
Route::get('/artajasa/upload', 'Artajasa\UploadFileController@index')->middleware('auth');
Route::post('/artajasa/upload', 'Artajasa\UploadFileController@doUploadFile')->middleware('auth');
Route::get('/artajasa/file', 'Artajasa\UploadFileController@file')->middleware('auth');

// REPORT ARTAJASA
Route::get('/artajasa/check', 'Artajasa\ReportSelisihController@check')->middleware('auth');
Route::get('/artajasa/report', 'Artajasa\ReportSelisihController@index')->middleware('auth');
Route::post('/artajasa/get-report', 'Artajasa\ReportSelisihController@getDataRekonsiliasi')->middleware('auth');

// LAPORAN PENJUALAN
Route::get('/laporan/penjualan', 'Laporan\PenjualanBulananController@index')->middleware('auth');
Route::post('/api/laporan/penjualan', 'Laporan\PenjualanBulananController@getSalesRepotMonthly')->middleware('auth');

